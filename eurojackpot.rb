class Eurojackpot
	def initialize (no1, no2, no3, no4, no5, b1, b2)
		@numbers = []
		@bonus = []
		@numbers.append(no1, no2, no3, no4, no5).sort
		@bonus.append(b1, b2).sort
	end

	def draw
		range1 = (1..50)
		range2 = (1..10)
		@drawn_no = range1.to_a.sample(5).sort
		@drawn_bonus = range2.to_a.sample(2).sort
	end

	def compare
		@count = 1
		until @numbers == @drawn_no && @bonus == @drawn_bonus
			draw
			@count += 1
		end
	end

	def jackpot
		compare
		years = @count / 52.to_f
		puts "Broj izvlačenja: #{@count}"
		puts "Broj godina: #{years.round(2)}"
	end


end

listic = Eurojackpot.new(8,17,36,42,48,5,7)
listic.jackpot
