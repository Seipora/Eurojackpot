# Eurojackpot

Kako bi se uvjerio da je teško izvući jackpot, uz pomoć Ruby programskog jezika programiraj simulaciju izvlačenja eurojackpota. Pravila su sljedeća:

- Igrač treba pogoditi 5 brojeva iz niza 1-50
- Igrač treba pogoditi 2 dodatna euro broja iz niza 1-10

Neka program izvlači brojeve dokle god jackpot ne bude izvučen i na kraju izbaci koliki broj izvlačenja je izvršeno. Također, izračunaj koliko godina je bilo potrebno za pogodak. Jednom tjedno izvlači se jackpot a godinu zaokruži na 52 tjedna.
